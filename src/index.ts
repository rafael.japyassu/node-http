import { createServer, IncomingMessage, ServerResponse } from 'http';
import { parse } from 'url';
import { peoples } from './database';
import { indexRoutes } from './routes';
import { peopleRoutes } from './routes/peoples';
import { HttpMethods } from './types';
import { addRoute, configResponse, routes } from './utils';

export type People = {
  name: string;
  age: number;
  cpf?: string;
}

export type Response<T> = {
  success: boolean;
  data: T | null | undefined;
  errors: string[];
}

async function toPromise(request: IncomingMessage): Promise<People> {
  return new Promise((resolve) => {
    let body = '';

    request.on('data', (chunck) => {
      body += chunck;
    });

    request.on('end', () => {
      const people: People = JSON.parse(body);
      resolve(people);
    });
  });
}

addRoute('/', indexRoutes);
addRoute('/peoples', peopleRoutes);

const server = createServer(async (request, response) => {
  // let statusCode = 200;
  // const json: Response<People> = {
  //   success: false,
  //   data: null,
  //   errors: []
  // };

  const { url, method } = request;
  const httpMethod = method as HttpMethods;

  if (url && method) {
    const service = routes[url][httpMethod];

    if (service) {
      service.handle(request, response);
    }
  }

  // const { query, pathname } = parse(request?.url || '', true);
  // const { name, age } = query;

  // if (pathname?.includes("/peoples")) {

  //   if (request?.method === "POST") {
  //     statusCode = 201;

  //     const json: Response<People> = {
  //       success: false,
  //       data: null,
  //       errors: []
  //     };

  //     const people = await toPromise(request);
  //     peoples.push(people);
  //     json.success = true;
  //     json.data = people;

  //     configResponse(statusCode, response, JSON.stringify(json));
  //   } else {
  //     if (pathname === "/peoples/find-by-cpf") {
  //       const json: Response<People> = {
  //         success: false,
  //         data: null,
  //         errors: []
  //       };
  
  //       const { cpf } = request.headers;
  //       const people = peoples.find(people => people?.cpf === cpf);
  
  //       if (!people) {
  //         json.errors.push(`Pessoa não encontrada para o cpf: ${cpf}`);
  //         statusCode = 400;
  //       } else {
  //         json.success = true;
  //         json.data = people;
  //       }

  //       configResponse(statusCode, response, JSON.stringify(json));
  //     } else if (pathname.includes("/peoples/"))  {
  //       const split = pathname.substring(1).split("/");
  //       const index = peoples.findIndex(people => people?.cpf === split[1]);
  //       if (request?.method === "DELETE") {
  //         if (split.length === 2) {
  //           peoples.splice(index, 1);
  //           statusCode = 204;

  //           configResponse(statusCode, response);
  //         }
  //       } else {
  //         const json: Response<People> = {
  //           success: false,
  //           data: null,
  //           errors: []
  //         };

  //         if (split.length !== 2) {
  //           json.errors = ['Rota não encontrada!']
  //           statusCode = 404;
  //         } else if (index < 0) {
  //           json.errors = ['Pessoa não encontrada!']
  //           statusCode = 400;
  //         } else {
  //           json.success = true;
  //           const people = peoples[index];
  //           json.data = people;
            
  //         }
  //         response.writeHead(statusCode, {
  //           'Content-Type': 'application/json'
  //         });
  //         response.write(JSON.stringify(json));
  //       }
  //     } 
  //     else {
        // const json: Response<People[]> = {
        //   success: true,
        //   data: peoples,
        //   errors: []
        // };
    
  //       response.writeHead(statusCode, {
  //         'Content-Type': 'application/json'
  //       });
  //       response.write(JSON.stringify(json));
  //     }
  //   }
  //   response.end();
  // } else {
  //   if (!name || !age) {
  //     statusCode = 422;
  //     json.errors.push('Informe os campos no query params corretos: name e age.');
  //   } else {
  //     const people: People = {
  //       name: String(name),
  //       age: Number(age)
  //     };
  
  //     json.success = true;
  //     json.data = people;
  //   }
  //   response.writeHead(statusCode, {
  //     'Content-Type': 'application/json'
  //   });
  //   response.write(JSON.stringify(json));
  //   response.end();
  // }
});

server.listen(3000, () => {
  console.log('API running in http://localhost:3000');
});

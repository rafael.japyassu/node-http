import { Call } from "../types";
import { Response, People } from '../index'
import { configResponse } from "../utils";
import { peoples } from "../database";

export const peopleRoutes: Call = {
  GET: {
    handle(request, response) {
      const json: Response<People[]> = {
        success: true,
        data: peoples,
        errors: []
      };

      configResponse(
        200,
        response,
        JSON.stringify(json)
      );
    },
  }
}
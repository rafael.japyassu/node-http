import { Call } from "../types";
import { configResponse } from "../utils";

export const indexRoutes: Call = {
  GET: {
    handle(request, response) {
      configResponse(
        204,
        response,
        'Raro Academy Node.js',
        { 'Content-Type': 'text/plain' }
      );
    },
  }
}
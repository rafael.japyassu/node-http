import { ServerResponse } from 'http';
import { Call, Route } from "./types";

export const routes = {} as Route;

export function addRoute(router: string, call: Call) {
  routes[router] = call;
}

export function configResponse(
  statusCode: number,
  response: ServerResponse,
  json: string = "",
  headers = {'Content-Type': 'application/json'}
) {
  if (statusCode === 204) {
    response.writeHead(statusCode);
  } else {
    response.writeHead(statusCode, headers);
    response.write(json);
  }
  response.end();
}
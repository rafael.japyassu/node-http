import { IncomingMessage, ServerResponse } from 'http';

export interface ICallAction {
  handle(request: IncomingMessage, response: ServerResponse): void;
}

export type HttpMethods = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';

export type Call = {
  [key in HttpMethods]?: ICallAction;
}

export type Route = {
  [key: string]: Call;
};